# Adipocyte Senescence

This repository contains the R markdown scripts used to statistically analyze some of the experimental data gathered by Kirsty Spalding's group (Karolinska Institute, Stockholm, Sweden) to investigate the links between obesity, hyperinsulinemia and adipocyte senescence.

The Rmd scripts of this repository are under the Creative Commons BY-NC-SA License. This license lets others remix, adapt, and build upon these scripts non-commercially, as long as they credit the authors (see AUTHORS file) and license their new creations under the identical terms.

The datasets (txt files) of this repository are under the Creative Commons BY-NC-ND Licence. Users can share only the unmodified datasets if they give credit to the authors and do not share it for commercial purposes. Users cannot make any additions, transformations or changes to the datasets under this license.

If you have questions regarding raw data, please contact kirsty.spalding@ki.se. 
If you have questions regarding these scripts, please contact carole.knibbe@inria.fr.
